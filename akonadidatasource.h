/*
    SPDX-FileCopyrightText: 2013 David Edmundson <davidedmundson@kde.org>

    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#ifndef AKONADIDATASOURCE_H
#define AKONADIDATASOURCE_H

#include <KPeopleBackend/BasePersonsDataSource>

#include <Akonadi/Monitor>

class AkonadiDataSource : public KPeople::BasePersonsDataSource
{
public:
    AkonadiDataSource(QObject *parent, const QVariantList &args = QVariantList());
    KPeople::AllContactsMonitor *createAllContactsMonitor() override;
    KPeople::ContactMonitor *createContactMonitor(const QString &contactUri) override;
    QString sourcePluginId() const override;

private:
    Akonadi::Monitor *m_monitor;
};

#endif // AKONADIDATASOURCE_H
