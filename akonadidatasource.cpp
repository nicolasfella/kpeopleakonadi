/*
    SPDX-FileCopyrightText: 2013 David Edmundson <davidedmundson@kde.org>
    SPDX-FileCopyrightText: 2021 Nicolas Fella <nicolas.fella@gmx.de>

    SPDX-License-Identifier: LGPL-2.1-or-later
*/

#include "akonadidatasource.h"

#include <QFile>

#include <Akonadi/Collection>
#include <Akonadi/CollectionFetchJob>
#include <Akonadi/CollectionFetchScope>
#include <Akonadi/Item>
#include <Akonadi/ItemFetchJob>
#include <Akonadi/ItemFetchScope>
#include <Akonadi/ServerManager>

#include <KLocalizedString>
#include <KPluginFactory>
#include <KPluginLoader>

#include <KContacts/Addressee>
#include <KContacts/VCardConverter>

#include "kpeoplebackend/abstracteditablecontact.h"

#include "kpeople_akonadi_plugin_debug.h"

using namespace Akonadi;
using namespace KPeople;

class VCardContact : public AbstractEditableContact
{
public:
    VCardContact()
    {
    }
    VCardContact(const KContacts::Addressee &addr, const QUrl &location)
        : m_addressee(addr)
        , m_location(location)
    {
    }
    void setAddressee(const KContacts::Addressee &addr)
    {
        m_addressee = addr;
    }
    void setUrl(const QUrl &url)
    {
        m_location = url;
    }

    QVariant customProperty(const QString &key) const override
    {
        QVariant ret;
        if (key == NameProperty) {
            const QString name = m_addressee.realName();
            if (!name.isEmpty()) {
                return name;
            }

            // If both first and last name are set combine them to a full name
            if (!m_addressee.givenName().isEmpty() && !m_addressee.familyName().isEmpty())
                return i18ndc("kpeoplevcard", "given-name family-name", "%1 %2", m_addressee.givenName(), m_addressee.familyName());

            // If only one of them is set just return what we know
            if (!m_addressee.givenName().isEmpty())
                return m_addressee.givenName();
            if (!m_addressee.familyName().isEmpty())
                return m_addressee.familyName();

            // Fall back to other identifiers
            if (!m_addressee.preferredEmail().isEmpty()) {
                return m_addressee.preferredEmail();
            }
            if (!m_addressee.phoneNumbers().isEmpty()) {
                return m_addressee.phoneNumbers().at(0).number();
            }
            return QVariant();
        } else if (key == EmailProperty)
            return m_addressee.preferredEmail();
        else if (key == AllEmailsProperty)
            return m_addressee.emails();
        else if (key == PictureProperty)
            return m_addressee.photo().data();
        else if (key == AllPhoneNumbersProperty) {
            const auto phoneNumbers = m_addressee.phoneNumbers();
            QVariantList numbers;
            for (const KContacts::PhoneNumber &phoneNumber : phoneNumbers) {
                // convert from KContacts specific format to QString
                numbers << phoneNumber.number();
            }
            return numbers;
        } else if (key == PhoneNumberProperty) {
            return m_addressee.phoneNumbers().isEmpty() ? QVariant() : m_addressee.phoneNumbers().at(0).number();
        } else if (key == VCardProperty) {
            KContacts::VCardConverter converter;
            return converter.createVCard(m_addressee);
        }

        return ret;
    }

    bool setCustomProperty(const QString &key, const QVariant &value) override
    {
        if (key == VCardProperty) {
            QFile f(m_location.toLocalFile());
            if (!f.open(QIODevice::WriteOnly))
                return false;
            f.write(value.toByteArray());
            return true;
        }
        return false;
    }

    static QString createUri(const QString &path)
    {
        return QStringLiteral("vcard:/") + path;
    }

private:
    KContacts::Addressee m_addressee;
    QUrl m_location;
};

class AkonadiAllContacts : public KPeople::AllContactsMonitor
{
    Q_OBJECT
public:
    AkonadiAllContacts();
    ~AkonadiAllContacts();
    QMap<QString, KPeople::AbstractContact::Ptr> contacts() override;
private Q_SLOTS:
    void onCollectionsFetched(KJob *job);
    void onItemsFetched(KJob *job);
    void onItemAdded(const Akonadi::Item &item);
    void onItemChanged(const Akonadi::Item &item);
    void onItemRemoved(const Akonadi::Item &item);
    void onServerStateChanged(Akonadi::ServerManager::State);

private:
    Akonadi::Monitor *m_monitor;
    QMap<QString, KPeople::AbstractContact::Ptr> m_contacts;
    int m_activeFetchJobsCount;
    bool m_fetchError;
};

AkonadiAllContacts::AkonadiAllContacts()
    : m_monitor(new Akonadi::Monitor(this))
    , m_activeFetchJobsCount(0)
    , m_fetchError(false)
{
    connect(Akonadi::ServerManager::self(), SIGNAL(stateChanged(Akonadi::ServerManager::State)), SLOT(onServerStateChanged(Akonadi::ServerManager::State)));
    onServerStateChanged(Akonadi::ServerManager::state());

    connect(m_monitor, SIGNAL(itemAdded(Akonadi::Item, Akonadi::Collection)), SLOT(onItemAdded(Akonadi::Item)));
    connect(m_monitor, SIGNAL(itemChanged(Akonadi::Item, QSet<QByteArray>)), SLOT(onItemChanged(Akonadi::Item)));
    connect(m_monitor, SIGNAL(itemRemoved(Akonadi::Item)), SLOT(onItemRemoved(Akonadi::Item)));

    m_monitor->setMimeTypeMonitored(QStringLiteral("text/directory"));
    m_monitor->itemFetchScope().fetchFullPayload();
    m_monitor->itemFetchScope().setFetchModificationTime(false);
    m_monitor->itemFetchScope().setFetchRemoteIdentification(false);

    CollectionFetchJob *fetchJob = new CollectionFetchJob(Collection::root(), CollectionFetchJob::Recursive, this);
    fetchJob->fetchScope().setContentMimeTypes(QStringList() << QStringLiteral("text/directory"));
    connect(fetchJob, SIGNAL(finished(KJob *)), SLOT(onCollectionsFetched(KJob *)));
}

AkonadiAllContacts::~AkonadiAllContacts()
{
}

QMap<QString, KPeople::AbstractContact::Ptr> AkonadiAllContacts::contacts()
{
    return m_contacts;
}

QString AkonadiDataSource::sourcePluginId() const
{
    return QStringLiteral("akonadi");
}

void AkonadiAllContacts::onItemAdded(const Item &item)
{
    if (!item.hasPayload<KContacts::Addressee>()) {
        return;
    }
    const QString id = item.url().toString();
    const KContacts::Addressee addressee = item.payload<KContacts::Addressee>();

    KPeople::AbstractContact::Ptr contact(new VCardContact(addressee, QUrl::fromLocalFile(QString())));

    m_contacts[id] = contact;
    Q_EMIT contactAdded(item.url().toString(), contact);
}

void AkonadiAllContacts::onItemChanged(const Item &item)
{
    if (!item.hasPayload<KPeople::AbstractContact::Ptr>()) {
        return;
    }
    const QString id = item.url().toString();
    const KPeople::AbstractContact::Ptr contact = item.payload<KPeople::AbstractContact::Ptr>();
    m_contacts[id] = contact;
    Q_EMIT contactChanged(item.url().toString(), contact);
}

void AkonadiAllContacts::onItemRemoved(const Item &item)
{
    if (!item.hasPayload<KPeople::AbstractContact::Ptr>()) {
        return;
    }
    const QString id = item.url().toString();
    m_contacts.remove(id);
    Q_EMIT contactRemoved(id);
}

// or we could add items as we go along...
void AkonadiAllContacts::onItemsFetched(KJob *job)
{
    if (job->error()) {
        qWarning() << job->errorString();
        m_fetchError = true;
    } else {
        ItemFetchJob *itemFetchJob = qobject_cast<ItemFetchJob *>(job);
        const auto items = itemFetchJob->items();
        for (const Item &item : items) {
            onItemAdded(item);
        }
    }

    if (--m_activeFetchJobsCount == 0 && !isInitialFetchComplete()) {
        emitInitialFetchComplete(true);
    }
}

void AkonadiAllContacts::onCollectionsFetched(KJob *job)
{
    if (job->error()) {
        qWarning() << job->errorString();
        emitInitialFetchComplete(false);
    } else {
        CollectionFetchJob *fetchJob = qobject_cast<CollectionFetchJob *>(job);
        QList<Collection> contactCollections;
        const auto collections = fetchJob->collections();
        for (const Collection &collection : collections) {
            // Skip virtual collections - we will get contacts linked into virtual
            // collections from their real parent collections
            if (collection.isVirtual()) {
                continue;
            }
            //             if (collection.contentMimeTypes().contains(KPeople::AbstractContact::Ptr::mimeType())) {
            ItemFetchJob *itemFetchJob = new ItemFetchJob(collection);
            itemFetchJob->fetchScope().fetchFullPayload();
            connect(itemFetchJob, SIGNAL(finished(KJob *)), SLOT(onItemsFetched(KJob *)));
            ++m_activeFetchJobsCount;
            //             }
        }
        if (m_activeFetchJobsCount == 0) {
            emitInitialFetchComplete(true);
        }
    }
    if (m_activeFetchJobsCount == 0 && !isInitialFetchComplete()) {
        emitInitialFetchComplete(true);
    }
}

void AkonadiAllContacts::onServerStateChanged(ServerManager::State state)
{
    // if we're broken tell kpeople we've loaded so kpeople doesn't block
    if (state == Akonadi::ServerManager::Broken && !isInitialFetchComplete()) {
        emitInitialFetchComplete(false);
        qWarning() << "Akonadi failed to load, some metacontact features may not be available";
        qWarning() << "For more information please load akonadi_console";
    }
}

class AkonadiContact : public KPeople::ContactMonitor
{
    Q_OBJECT
public:
    AkonadiContact(Akonadi::Monitor *monitor, const QString &contactUri);
    ~AkonadiContact();
private Q_SLOTS:
    void onContactFetched(KJob *);
    void onContactChanged(const Akonadi::Item &);

private:
    Akonadi::Monitor *m_monitor;
    Akonadi::Item m_item;
};

AkonadiContact::AkonadiContact(Akonadi::Monitor *monitor, const QString &contactUri)
    : ContactMonitor(contactUri)
    , m_monitor(monitor)
{
    // TODO: optimiZation, base class could copy across from the model if the model exists
    // then we should check if contact is already set to something and avoid the initial fetch

    // load the contact initially
    m_item = Item::fromUrl(QUrl(contactUri));
    ItemFetchJob *itemFetchJob = new ItemFetchJob(m_item);
    itemFetchJob->fetchScope().fetchFullPayload();
    connect(itemFetchJob, SIGNAL(finished(KJob *)), SLOT(onContactFetched(KJob *)));

    // then watch for that item changing
    m_monitor->setItemMonitored(m_item, true);
    connect(m_monitor, SIGNAL(itemChanged(Akonadi::Item, QSet<QByteArray>)), SLOT(onContactChanged(Akonadi::Item)));
}

AkonadiContact::~AkonadiContact()
{
    m_monitor->setItemMonitored(m_item, false);
}

void AkonadiContact::onContactFetched(KJob *job)
{
    ItemFetchJob *fetchJob = qobject_cast<ItemFetchJob *>(job);
    if (fetchJob->items().count() && fetchJob->items().first().hasPayload<KPeople::AbstractContact::Ptr>()) {
        setContact(fetchJob->items().first().payload<KPeople::AbstractContact::Ptr>());
    }
}

void AkonadiContact::onContactChanged(const Item &item)
{
    if (item != m_item) {
        return;
    }
    if (!item.hasPayload<KPeople::AbstractContact::Ptr>()) {
        return;
    }
    setContact(item.payload<KPeople::AbstractContact::Ptr>());
}

AkonadiDataSource::AkonadiDataSource(QObject *parent, const QVariantList &args)
    : BasePersonsDataSource(parent)
    , m_monitor(new Akonadi::Monitor(this))
{
    Q_UNUSED(args);
    m_monitor->itemFetchScope().fetchFullPayload();
    m_monitor->itemFetchScope().setFetchModificationTime(false);
    m_monitor->itemFetchScope().setFetchRemoteIdentification(false);
}

KPeople::AllContactsMonitor *AkonadiDataSource::createAllContactsMonitor()
{
    return new AkonadiAllContacts();
}

KPeople::ContactMonitor *AkonadiDataSource::createContactMonitor(const QString &contactUri)
{
    return new AkonadiContact(m_monitor, contactUri);
}

K_PLUGIN_CLASS_WITH_JSON(AkonadiDataSource, "akonadi_kpeople_plugin.json")

#include "akonadidatasource.moc"
